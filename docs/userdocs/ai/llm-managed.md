## Public managed LLM

The NRP managed LLM is running at [https://nrp-llama3.nrp-nautilus.io](https://nrp-llama3.nrp-nautilus.io), API at [https://nrp-llama3-api.nrp-nautilus.io](https://nrp-llama3-api.nrp-nautilus.io)