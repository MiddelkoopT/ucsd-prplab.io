## Cluster usage by node and namespace

!!! note "Additional Features available directly on website"

    Additional features, such as a downloadable table, can be viewed on the <a href="https://observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace">ObservableHQ Notebook.</a>

This is the experimental page for querying the monitoring information for nodes usage. Still work in progress. Data gathered since 09-15-23. Additional features, such as a downloadable table, can be viewed <a href="https://observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace">here</a>.
<div id="observablehq-viewof-form-c3c8e66e"></div>
<div id="observablehq-viewof-nodeName-c3c8e66e"></div>
<div id="observablehq-viewof-zone-c3c8e66e"></div>
<div id="observablehq-viewof-region-c3c8e66e"></div>
<div id="observablehq-statusMessage-c3c8e66e"></div>
<div id="observablehq-viewof-gpuChart-c3c8e66e"></div>
<div id="observablehq-viewof-cpuChart-c3c8e66e"></div>
<div id="observablehq-viewof-memoryChart-c3c8e66e"></div>
<div id="observablehq-viewof-storageChart-c3c8e66e"></div>
<div id="observablehq-viewof-fpgaChart-c3c8e66e"></div>
<div id="observablehq-datatable-c3c8e66e"></div>
<p>Credit: <a href="https://observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace">Node Allocated Resources (By Namespace) by NRP Nautilus</a></p>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@observablehq/inspector@5/dist/inspector.css">
<script type="module">
import {Runtime, Inspector} from "https://cdn.jsdelivr.net/npm/@observablehq/runtime@5/dist/runtime.js";
import define from "https://api.observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace.js?v=4";
new Runtime().module(define, name => {
  if (name === "viewof form") return new Inspector(document.querySelector("#observablehq-viewof-form-c3c8e66e"));
  if (name === "viewof nodeName") return new Inspector(document.querySelector("#observablehq-viewof-nodeName-c3c8e66e"));
  if (name === "viewof zone") return new Inspector(document.querySelector("#observablehq-viewof-zone-c3c8e66e"));
  if (name === "viewof region") return new Inspector(document.querySelector("#observablehq-viewof-region-c3c8e66e"));
  if (name === "statusMessage") return new Inspector(document.querySelector("#observablehq-statusMessage-c3c8e66e"));
  if (name === "viewof gpuChart") return new Inspector(document.querySelector("#observablehq-viewof-gpuChart-c3c8e66e"));
  if (name === "viewof cpuChart") return new Inspector(document.querySelector("#observablehq-viewof-cpuChart-c3c8e66e"));
  if (name === "viewof memoryChart") return new Inspector(document.querySelector("#observablehq-viewof-memoryChart-c3c8e66e"));
  if (name === "viewof storageChart") return new Inspector(document.querySelector("#observablehq-viewof-storageChart-c3c8e66e"));
  if (name === "viewof fpgaChart") return new Inspector(document.querySelector("#observablehq-viewof-fpgaChart-c3c8e66e"));
  return ["data"].includes(name);
});
</script>
