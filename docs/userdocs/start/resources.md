Although you can run your own containers, there are several services and resources 
already deployed by cluster admins that you can use without creating those yourself.

:exclamation: Most services require additional registration.

**People are expected to report any not working service in [Matrix Support](/userdocs/start/contact).**

#### Computations
- [JupyterHub (West Coast)][1]
- [WebODM (Web Open Drone Map): Drone Images stitching][4]

#### Data sharing and collaboration tools
- [EtherPad: notebooks][3] 
- [GitLab: code and containers repository][5]
- [Jitsi: Video conferencing][10]
- [Nextcloud: File sharing][6]
- [Overleaf: LaTeX collaboration][12]
- [SyncThing: File sync][8] ([Contact us to set up](/userdocs/start/contact))
- [Coder: remote development environment][13]
- [Hedgedoc: collaborative markdown editor][14]

#### Network monitoring
- [Traceroute tool][7]
- [PerfSONAR][11]

[1]: https://jupyterhub-west.nrp-nautilus.io/
[3]: https://etherpad.nrp-nautilus.io/
[4]: https://webodm.nrp-nautilus.io/
[5]: https://gitlab.nrp-nautilus.io/
[6]: https://nextcloud.nrp-nautilus.io/
[7]: https://traceroute.nrp-nautilus.io/
[8]: https://syncthing.net
[10]: https://jitsi.nrp-nautilus.io
[11]: https://perfsonar.nrp-nautilus.io/maddash-webui/
[12]: https://overleaf.nrp-nautilus.io
[13]: https://coder.nrp-nautilus.io
[14]: https://hedgedoc.nrp-nautilus.io