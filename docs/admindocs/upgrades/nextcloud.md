#### Upgrading nextcloud

Before upgrading read the [guide](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/upgrade.html). Make sure no database version upgrade is required.

The image for the nextcloud is set to major version. To upgrade, change the image to the next major, and wait for restarted pod to complete the upgrade.

After the upgrade need to run the [long running steps](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/upgrade.html#long-running-migration-steps) manually.