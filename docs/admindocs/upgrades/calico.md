#### Upgrading calico

Follow the [official guide](https://docs.tigera.io/calico/latest/operations/upgrading/kubernetes-upgrade#upgrading-an-installation-that-uses-manifests-and-the-kubernetes-api-datastore).

The manifest must be changed according to the [current calico repo](https://gitlab.nrp-nautilus.io/prp/calico), and repo has to be updated with the changes.